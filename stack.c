#include <stdio.h>
#include <stdlib.h>
#include "stack.h"


/**
 *\brief   détermine si une pile est vide
 *\param   s     la pile dont on veut savoir si elle est vide
 *\return  0 si la pile est vide et 1 sinon
*/
int is_empty(stack s){
  return (s == NULL);
}

/**
 *\brief   place un caractère au sommet de la pile
 *\param   s     la pile sur laquelle on place le caractère
 *\param   e le char à placer sur la pile
*/
void push(char e, stack *s){
  stack r = (stack) malloc (sizeof(struct maillon));
  r->data = e;
  r->previous = *s;
  *s = r;
}

/**
 *\brief   retire le premier élement de la pile et renvoit sa valeur
 *\param   s     la pile dont on retire l'élement
 *\return  la veleur du char retiré
*/
char pop(stack *s){
  stack tmp = *s;
  char color = (*s)->data;
  *s = (*s)->previous;
  free(tmp);
  return color;
}

/**
 *\brief   calcule la hauteur d'une pile
 *\param   s     la pile dont on cherche la hauteur
 *\return  la hauteur de la pile
*/
int depth(stack s){
  int l=0;
  while (s != 0){
    l=l+1;
    s=s->previous;
  }
  return l;
}

/**
 *\brief   affiche une pile
 *\param   s     la pile à afficher
*/
void stack_print(stack s){
  while (!is_empty(s)){
    printf ("[%c]\n", s->data);
    s=s->previous;
  }
  printf("---\n");
}

/**
 *\brief   vide une pile
 *\param   s     la pile à vider
*/
void stack_empty(stack * s)
{
    while (!is_empty(*s))
        pop(s);
}

/**
 *\brief   inverse l'ordre des élements d'une pile
 *\param   s     la pile à inverser
 *\return  la pile renversée
*/
stack reverse(stack s) {
  stack rev = NULL;
  while (!is_empty(s))
  {
    push(s->data,&rev);
    s=s->previous;
  }
  return rev;
}

