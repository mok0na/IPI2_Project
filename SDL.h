#ifndef SDL_H
#define SDL_H

#include "grid.h"

void drawRectangle(SDL_Surface *ecran, int px, int py, int size, int r, int g, int b);
void fillScreen(SDL_Surface *ecran, int r, int g, int b);
void toRGB(int* r, int* g, int* b, grid grid, int x, int y);
void toRGB2(int* r, int* g, int* b, char c);
char toChar (int r, int g, int b);
void drawGrid(SDL_Surface *ecran, grid grid, int size);
void updateGrid(SDL_Surface *ecran, grid coord, grid grid, int size);
int checkClick(SDL_MouseButtonEvent event,int x,int hx,int y,int hy);

#endif
