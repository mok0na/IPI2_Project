#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "constant.h"
#include "SDL.h"
#include "game.h"

/**
 *\brief   Affiche la barre d'options
 *\details  "Lien" cliquables pour plusieurs options
 *\param   ecran SDL_Surface sur laquelle est affichée la barre
*/
void option(SDL_Surface *ecran)
{
	SDL_Surface *option;
	SDL_Rect position;
    SDL_Event event;

    int continuer = 1;

    option = IMG_Load("img/option.jpg");
    position.x=0;
    position.y=0;
	SDL_BlitSurface(option, NULL, ecran, &position);
    SDL_Flip(ecran);

    while(continuer)
    {
    	SDL_WaitEvent(&event);
    	switch(event.type) {
    		case SDL_QUIT:
        		continuer=0;
        	break;

        	case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                	case SDLK_q:
                		continuer = 0;
                        break;
                    case SDLK_ESCAPE:
                    	continuer = 0;
                    	break;

                    default: break;
                }
                break;



        	case SDL_MOUSEBUTTONDOWN:
	        	if(event.button.button == SDL_BUTTON_LEFT)
	        	{
	        		/*12x12*/
	            	if (checkClick(event.button,220,530,130,270))
	            	{
	            		play(ecran,12);
	            		continuer=0;

	            	}
	            	/*18x18*/
	            	else if (checkClick(event.button,220,530,300,430))
	            	{
	              		play(ecran,18);
	              		continuer=0;
	            	}
	            	/*24x24*/
	            	else if (checkClick(event.button,220,530,460,600))
	            	{
	              		play(ecran,24);
	              		continuer=0;
	            	}
	            	printf(" option == 1 menu boucle quit");
	            }
	        break;
	    }

		
    }
    SDL_FreeSurface(option);
}
