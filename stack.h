#ifndef LIST_H
#define LIST_H

typedef struct maillon* stack;

struct maillon {
  char data;
  stack previous;
};

int is_empty(stack s);
void push(char c, stack *s);
char pop(stack *s);
int depth(stack s);
void stack_print(stack s);
void stack_empty(stack * s);
stack reverse(stack s);

#endif
