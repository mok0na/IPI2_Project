#ifndef TREE_H
#define TREE_H

#include "stack.h"
#include "grid.h"


typedef struct node * tree;

typedef struct node
{
    grid g;
    int sz;
    tree sons[5];
} node;

tree createTree(grid g, int sz);

int isEmptyTree(tree t);
void emptyTree(tree * t);

#endif

