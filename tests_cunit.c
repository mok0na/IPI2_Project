#include <stdio.h>
#include <stdlib.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

#include "grid.h"



/**
 *\file cunit.c
 *\brief Tests sur les fonctions de grid
 *\author ...
 *\version 0.1
 *\date
 *
 */








void test_init_grid_n(int n){

  int i, j;
  grid g = init_grid(n);

  /* On verifie la taille de la grille */
  CU_ASSERT(g.size == n);
  

  /* On verifie que la grille prend bien la taille memoire d'une grille et qu'elle est bien enregistree en memoire */
  CU_ASSERT(sizeof(struct grid) == sizeof g);
  CU_ASSERT(&g != NULL);

  for (i = 0 ; i<n ; i++){
    /* Taille memoire et affectation effectuee */
    CU_ASSERT(sizeof g.m[i] == sizeof (char*));
    CU_ASSERT(&(g.m[i]) != NULL);

    for (j = 0 ; j<n ; j++){
      /* Taille memoire*/
      CU_ASSERT(sizeof g.m[i][j] == sizeof(char));
      /* On verifie que les différentes cases sont bien affectées à un espace memoire */
      CU_ASSERT(&(g.m[i][j]) != NULL);
    }
  }
  empty(&g);
}



void test_init_grid(void){
  
  printf("Tests init_grid\n\n\n");
  
  grid g; 
  /* n = -1 renvoie une erreur */ 
  printf("Le test renvoie renvoie Error:n<0 :\n");
  g = init_grid(-1);
  CU_ASSERT(g.size == 0);
  CU_ASSERT_PTR_NULL(g.m);

  /* n = 0 renvoie une erreur */
  printf("Le test renvoie renvoie Error:n<0 :\n");
  g = init_grid(0);
  CU_ASSERT(g.size == 0);
  CU_ASSERT_PTR_NULL(g.m);

  empty(&g);
  /* n = 1 */
  test_init_grid_n(1);

  /* n = 12 */
  test_init_grid_n(12);

  /* n = 18 */
  test_init_grid_n(18);

  /* n = 24 */
  test_init_grid_n(24);

  /* n = 100 */
  test_init_grid_n(100);
}




void test_rand_init(void){


  printf("Tests rand_init\n\n\n");
  

  int n = 10 ;
  grid g = rand_init(n);
  

  /* On vérifie si les valeurs stockées dans les cases sont bien des couleurs */
  for (int i=0; i<n ; i++){
    for (int j=0 ; j<n ; j++){
      CU_ASSERT(g.m[i][j] == 'B'
	     || g.m[i][j] == 'V'
	     || g.m[i][j] == 'R'
	     || g.m[i][j] == 'J'
	     || g.m[i][j] == 'M'
	     || g.m[i][j] == 'G');
    }
  }
  empty(&g);
}


/* Empty est testé par valgrind à chaque fois que l'on utilise la fonction */



void test_setColorAt(void){


  printf("Tests setColorAt\n\n\n");
  

  char couleurs[6] = {'B', 'V', 'R', 'J', 'M', 'G'};
  
  /* Le fichier "testsetColorAt" contient une grille avec une case de chaque couleur */ 
  grid g = file_init("fichiers_test/testsetColorAt");
  int i,j,l;
  for (l=0 ; l<6 ; l++){
    for (i=0 ; i<g.size ; i++){
      for (j=0 ; j<g.size ; j++){
        CU_ASSERT(setColorAt(i,j,couleurs[l],&g) == 0);
        CU_ASSERT(g.m[i][j] == couleurs[l]);
        CU_ASSERT(setColorAt(i,j,'y',&g) == 0);
        CU_ASSERT(g.m[i][j] == 'y');
        CU_ASSERT(setColorAt(i,j,'n',&g) == 0);
        CU_ASSERT(g.m[i][j] == 'n');
      }
    } 
  }

  /* test pour changer vers une couleur invalide */
  /* Affichera "couleur non valide" */
  CU_ASSERT(setColorAt(0,0,'1',&g) == 1);

  /* On essaie de changer une case qui n'est pas dans la grille */
  /* Affichera "x>=n ou y >=n"*/
  CU_ASSERT(setColorAt(g.size,g.size,'V',&g) == 2);

  empty(&g);
   
 
}




void test_checkForWin(void){
   

  printf("Tests checkForWin\n\n\n");
  


  /* Les grilles ont toutes leurs cases de couleurs identiques */
  /* car a l'avant derniere etape, il ne manque qu'une couleur a modifier */
  /* avant que l'on gagne. (Et si c'est bon pour chaque case, vu qu'on  */
  /* les change toutes l'une après l'autre, cela ne posera pas de soucis) */

  char couleurs[6] = {'B', 'V', 'R', 'J', 'M', 'G'};
  
  /* Grille bleue */

  
  /* On change une case a chaque fois*/
  for (int k=0 ; k<6 ; k++){
    for (int i=0 ; i<10 ; i++){
      for (int j=0 ; j<10 ; j++){
        grid g = file_init("fichiers_test/testcheckForWin_B");
  
        setColorAt(i,j,couleurs[k],&g);
        if (k == 0){
          CU_ASSERT(checkForWin(g) == 1);
        }
        else{
          CU_ASSERT(checkForWin(g) == 0);
        }
        empty(&g);
      }
    }
  }
  
  

  /* Grille verte */
  
  /* On change une case a chaque fois*/
  for (int k=0 ; k<6 ; k++){
    for (int i=0 ; i<10 ; i++){
      for (int j=0 ; j<10 ; j++){
        
        grid g = file_init("fichiers_test/testcheckForWin_V");
  
        setColorAt(i,j,couleurs[k],&g);
        if (k == 1){
          CU_ASSERT(checkForWin(g) == 1);
        }
        else{
          CU_ASSERT(checkForWin(g) == 0);
        }
        empty(&g);
      }
    }
  }
 


  /* Grille rouge */
  
  /* On change une case a chaque fois*/
  for (int k=0 ; k<6 ; k++){
    for (int i=0 ; i<10 ; i++){
      for (int j=0 ; j<10 ; j++){
        
        grid g = file_init("fichiers_test/testcheckForWin_R");
  
        setColorAt(i,j,couleurs[k],&g);
        if (k == 2){
          CU_ASSERT(checkForWin(g) == 1);
        }
        else{
          CU_ASSERT(checkForWin(g) == 0);
        }
        empty(&g);
      }
    }
  }
 


  /* Grille jaune */
  
  /* On change une case a chaque fois*/
  for (int k=0 ; k<6 ; k++){
    for (int i=0 ; i<10 ; i++){
      for (int j=0 ; j<10 ; j++){
        
        grid g = file_init("fichiers_test/testcheckForWin_J");
  
        setColorAt(i,j,couleurs[k],&g);
        if (k == 3){
          CU_ASSERT(checkForWin(g) == 1);
        }
        else{
          CU_ASSERT(checkForWin(g) == 0);
        }
        empty(&g);
      }
    }
  }
 

  /* Grille marron */
   
  /* On change une case a chaque fois*/
  for (int k=0 ; k<6 ; k++){
    for (int i=0 ; i<10 ; i++){
      for (int j=0 ; j<10 ; j++){
        
        grid g = file_init("fichiers_test/testcheckForWin_M");
  
        setColorAt(i,j,couleurs[k],&g);
        if (k == 4){
          CU_ASSERT(checkForWin(g) == 1);
        }
        else{
          CU_ASSERT(checkForWin(g) == 0);
        }
        empty(&g);
      }
    }
  }
 

  /* Grille grise */
  
  /* On change une case a chaque fois*/
  for (int k=0 ; k<6 ; k++){
    for (int i=0 ; i<10 ; i++){
      for (int j=0 ; j<10 ; j++){
        
        grid g = file_init("fichiers_test/testcheckForWin_G");
  
        setColorAt(i,j,couleurs[k],&g);
        if (k == 5){
          CU_ASSERT(checkForWin(g) == 1);
        }
        else{
          CU_ASSERT(checkForWin(g) == 0);
        }
        empty(&g);
      }
    }
  }
 

  /* Grille avec des valeurs valides ainsi que des valeurs invalides */
  grid g = file_init("fichiers_test/testcheckForWin_other");
  CU_ASSERT(checkForWin(g) == 2);
  empty(&g);

  /* Grille avec toutes les cases de la même couleur invalide */
  g = file_init("fichiers_test/testcheckForWin_other2");
  CU_ASSERT(checkForWin(g) == 2);
  empty(&g);

}


void test_find4Connexe(void){
  
 
  printf("Tests find4Connexe\n\n\n");
  

  /* Premiere grille */  

  create_file("B R R\nB R B\nB B B\n","fichiers_test/test_connexe1");
  grid g = file_init("fichiers_test/test_connexe1");

  grid coord = init_grid(g.size); 

  int i,j;
  for (i=0; i<g.size; i++){
    for (j=0; j<g.size; j++){
      setColorAt(i,j,'n',&coord);
    }
  }

  CU_ASSERT(find4Connexe(g,&coord,0,0,'G') == 1);
  
  CU_ASSERT(coord.m[0][0] == 'y');
  CU_ASSERT(coord.m[0][1] == 'c');
  CU_ASSERT(coord.m[0][2] == 'c');
  CU_ASSERT(coord.m[1][0] == 'y');
  CU_ASSERT(coord.m[1][1] == 'c');
  CU_ASSERT(coord.m[1][2] == 'y');
  CU_ASSERT(coord.m[2][0] == 'y');
  CU_ASSERT(coord.m[2][1] == 'y');
  CU_ASSERT(coord.m[2][2] == 'y');

  empty(&g);
 

  /* Deuxieme grille */  

  create_file("B B B\nR B R\nR B B\n","fichiers_test/test_connexe2");
  g = file_init("fichiers_test/test_connexe2"); 

  for (i=0; i<g.size; i++){
    for (j=0; j<g.size; j++){
      setColorAt(i,j,'n',&coord);
    }
  }

  CU_ASSERT(find4Connexe(g,&coord,0,0,'G') == 1);

  CU_ASSERT(coord.m[0][0] == 'y');
  CU_ASSERT(coord.m[0][1] == 'y');
  CU_ASSERT(coord.m[0][2] == 'y');
  CU_ASSERT(coord.m[1][0] == 'c');
  CU_ASSERT(coord.m[1][1] == 'y');
  CU_ASSERT(coord.m[1][2] == 'c');
  CU_ASSERT(coord.m[2][0] == 'c');
  CU_ASSERT(coord.m[2][1] == 'y');
  CU_ASSERT(coord.m[2][2] == 'y');

  empty(&g);
 

  /* Troisieme grille */  
  /* La fonction s'arrete d'iterer apres la case (0,0) donc toutes les autres cases restent avec un 'n' et non un 'c' */

  create_file("B R B\nR B B\nB B B\n","fichiers_test/test_connexe3");
  g = file_init("fichiers_test/test_connexe3"); 

  for (i=0; i<g.size; i++){
    for (j=0; j<g.size; j++){
      setColorAt(i,j,'n',&coord);
    }
  }

  CU_ASSERT(find4Connexe(g,&coord,0,0,'G') == 1);

  CU_ASSERT(coord.m[0][0] == 'y');
  CU_ASSERT(coord.m[0][1] == 'c');
  CU_ASSERT(coord.m[0][2] == 'n');
  CU_ASSERT(coord.m[1][0] == 'c');
  CU_ASSERT(coord.m[1][1] == 'n');
  CU_ASSERT(coord.m[1][2] == 'n');
  CU_ASSERT(coord.m[2][0] == 'n');
  CU_ASSERT(coord.m[2][1] == 'n');
  CU_ASSERT(coord.m[2][2] == 'n');

  empty(&g);
 
 
  /* Quatrieme grille */  

  create_file("B B B\nB R B\nB B B\n","fichiers_test/test_connexe4");
  g = file_init("fichiers_test/test_connexe4"); 

  for (i=0; i<g.size; i++){
    for (j=0; j<g.size; j++){
      setColorAt(i,j,'n',&coord);
    }
  }

  CU_ASSERT(find4Connexe(g,&coord,0,0,'G') == 1);

  CU_ASSERT(coord.m[0][0] == 'y');
  CU_ASSERT(coord.m[0][1] == 'y');
  CU_ASSERT(coord.m[0][2] == 'y');
  CU_ASSERT(coord.m[1][0] == 'y');
  CU_ASSERT(coord.m[1][1] == 'c');
  CU_ASSERT(coord.m[1][2] == 'y');
  CU_ASSERT(coord.m[2][0] == 'y');
  CU_ASSERT(coord.m[2][1] == 'y');
  CU_ASSERT(coord.m[2][2] == 'y');

  empty(&g);
 
 
  /* Cinquieme grille */  

  create_file("B B B B B\nR R R R B\nB B B R B\nB R R R B\nB B B B B\n","fichiers_test/test_connexe5");
  g = file_init("fichiers_test/test_connexe5"); 

  empty(&coord);
  
  coord = init_grid(g.size); 


  for (i=0; i<g.size; i++){
    for (j=0; j<g.size; j++){
      setColorAt(i,j,'n',&coord);
    }
  }

  CU_ASSERT(find4Connexe(g,&coord,0,0,'G') == 1);

  CU_ASSERT(coord.m[0][0] == 'y');
  CU_ASSERT(coord.m[0][1] == 'y');
  CU_ASSERT(coord.m[0][2] == 'y');
  CU_ASSERT(coord.m[0][3] == 'y');
  CU_ASSERT(coord.m[0][4] == 'y');
  CU_ASSERT(coord.m[1][0] == 'c');
  CU_ASSERT(coord.m[1][1] == 'c');
  CU_ASSERT(coord.m[1][2] == 'c');
  CU_ASSERT(coord.m[1][3] == 'c');
  CU_ASSERT(coord.m[1][4] == 'y');
  CU_ASSERT(coord.m[2][0] == 'y');
  CU_ASSERT(coord.m[2][1] == 'y');
  CU_ASSERT(coord.m[2][2] == 'y');
  CU_ASSERT(coord.m[2][3] == 'c');
  CU_ASSERT(coord.m[2][4] == 'y');
  CU_ASSERT(coord.m[3][0] == 'y');
  CU_ASSERT(coord.m[3][1] == 'c');
  CU_ASSERT(coord.m[3][2] == 'c');
  CU_ASSERT(coord.m[3][3] == 'c');
  CU_ASSERT(coord.m[3][4] == 'y');
  CU_ASSERT(coord.m[4][0] == 'y');
  CU_ASSERT(coord.m[4][1] == 'y');
  CU_ASSERT(coord.m[4][2] == 'y');
  CU_ASSERT(coord.m[4][3] == 'y');
  CU_ASSERT(coord.m[4][4] == 'y');



  empty(&g);
  


  empty(&coord);

}





void test_changeColor(void){


  printf("Tests changeColor\n\n\n");
  



  /* La grille de test est la suivante :
B V R
B B G
M J B
*/

  create_file("B V R\nB B G\nM J B\n","fichiers_test/test_changecolor1");
 
  /* cas ou colorNew n'est pas une couleur valide */
  /* N'apparait pas car find4Connexe renvoie une erreur dans ce cas */
  
 
  /* cas ou colorNew est une couleur (6 cas) */
  grid g = file_init("fichiers_test/test_changecolor1");

  grid coord = init_grid(g.size); 
  int i,j;
  for (i=0; i<g.size; i++){
    for (j=0; j<g.size; j++){
      setColorAt(i,j,'n',&coord);
    }
  }

  /* Changement vers le vert */

  find4Connexe(g,&coord,0,0,'V');

  CU_ASSERT(changeColor(&g,coord,'V') == 0); 
  changeColor(&g,coord,'V');
  CU_ASSERT(g.m[0][0] == 'V');  
  CU_ASSERT(g.m[0][1] == 'V');  
  CU_ASSERT(g.m[0][2] == 'R');  
  CU_ASSERT(g.m[1][0] == 'V');  
  CU_ASSERT(g.m[1][1] == 'V');  
  CU_ASSERT(g.m[1][2] == 'G');  
  CU_ASSERT(g.m[2][0] == 'M');  
  CU_ASSERT(g.m[2][1] == 'J');  
  CU_ASSERT(g.m[2][2] == 'B');  

  empty(&g);

  /* Vers le rouge */

  g = file_init("fichiers_test/test_changecolor1");
  
  find4Connexe(g,&coord,0,0,'R');

  CU_ASSERT(changeColor(&g,coord,'R') == 0); 
  CU_ASSERT(g.m[0][0] == 'R');  
  CU_ASSERT(g.m[0][1] == 'V');  
  CU_ASSERT(g.m[0][2] == 'R');  
  CU_ASSERT(g.m[1][0] == 'R');  
  CU_ASSERT(g.m[1][1] == 'R');  
  CU_ASSERT(g.m[1][2] == 'G');  
  CU_ASSERT(g.m[2][0] == 'M');  
  CU_ASSERT(g.m[2][1] == 'J');  
  CU_ASSERT(g.m[2][2] == 'B');  

  empty(&g);

  /* Vers le bleu */

  g = file_init("fichiers_test/test_changecolor1");

  find4Connexe(g,&coord,0,0,'B');

  CU_ASSERT(changeColor(&g,coord,'B') == 0); 
  CU_ASSERT(g.m[0][0] == 'B');  
  CU_ASSERT(g.m[0][1] == 'V');  
  CU_ASSERT(g.m[0][2] == 'R');  
  CU_ASSERT(g.m[1][0] == 'B');  
  CU_ASSERT(g.m[1][1] == 'B');  
  CU_ASSERT(g.m[1][2] == 'G');  
  CU_ASSERT(g.m[2][0] == 'M');  
  CU_ASSERT(g.m[2][1] == 'J');  
  CU_ASSERT(g.m[2][2] == 'B');  

  empty(&g);

  /* Vers le jaune */

  g = file_init("fichiers_test/test_changecolor1");

  find4Connexe(g,&coord,0,0,'J');

  CU_ASSERT(changeColor(&g,coord,'J') == 0); 
  CU_ASSERT(g.m[0][0] == 'J');  
  CU_ASSERT(g.m[0][1] == 'V');  
  CU_ASSERT(g.m[0][2] == 'R');  
  CU_ASSERT(g.m[1][0] == 'J');  
  CU_ASSERT(g.m[1][1] == 'J');  
  CU_ASSERT(g.m[1][2] == 'G');  
  CU_ASSERT(g.m[2][0] == 'M');  
  CU_ASSERT(g.m[2][1] == 'J');  
  CU_ASSERT(g.m[2][2] == 'B');  

  empty(&g);

  /* Vers le marron */

  g = file_init("fichiers_test/test_changecolor1");

  find4Connexe(g,&coord,0,0,'M');

  CU_ASSERT(changeColor(&g,coord,'M') == 0); 
  CU_ASSERT(g.m[0][0] == 'M');  
  CU_ASSERT(g.m[0][1] == 'V');  
  CU_ASSERT(g.m[0][2] == 'R');  
  CU_ASSERT(g.m[1][0] == 'M');  
  CU_ASSERT(g.m[1][1] == 'M');  
  CU_ASSERT(g.m[1][2] == 'G');  
  CU_ASSERT(g.m[2][0] == 'M');  
  CU_ASSERT(g.m[2][1] == 'J');  
  CU_ASSERT(g.m[2][2] == 'B');  

  empty(&g);

  /* Vers le gris */

  g = file_init("fichiers_test/test_changecolor1");

  coord.m[0][0] == 'y';
  coord.m[0][1] == 'n';
  coord.m[0][2] == 'n';
  coord.m[1][0] == 'y';
  coord.m[1][1] == 'y';
  coord.m[1][2] == 'y';
  coord.m[2][0] == 'n';
  coord.m[2][1] == 'n';
  coord.m[2][2] == 'n';
 
  CU_ASSERT(changeColor(&g,coord,'G') == 0); 
  CU_ASSERT(g.m[0][0] == 'G');  
  CU_ASSERT(g.m[0][1] == 'V');  
  CU_ASSERT(g.m[0][2] == 'R');  
  CU_ASSERT(g.m[1][0] == 'G');  
  CU_ASSERT(g.m[1][1] == 'G');  
  CU_ASSERT(g.m[1][2] == 'G');  
  CU_ASSERT(g.m[2][0] == 'M');  
  CU_ASSERT(g.m[2][1] == 'J');  
  CU_ASSERT(g.m[2][2] == 'B');  

  empty(&g);



  /* cas ou coord ne contient que des 'y' */

  coord.m[0][0] = 'y';  
  coord.m[0][1] = 'y';  
  coord.m[0][2] = 'y';  
  coord.m[1][0] = 'y';  
  coord.m[1][1] = 'y';  
  coord.m[1][2] = 'y';  
  coord.m[2][0] = 'y';  
  coord.m[2][1] = 'y';  
  coord.m[2][2] = 'y';  
   
  g = file_init("fichiers_test/test_changecolor1");

  CU_ASSERT(changeColor(&g,coord,'R') == 0); 
  CU_ASSERT(g.m[0][0] == 'R');  
  CU_ASSERT(g.m[0][1] == 'R');  
  CU_ASSERT(g.m[0][2] == 'R');  
  CU_ASSERT(g.m[1][0] == 'R');  
  CU_ASSERT(g.m[1][1] == 'R');  
  CU_ASSERT(g.m[1][2] == 'R');  
  CU_ASSERT(g.m[2][0] == 'R');  
  CU_ASSERT(g.m[2][1] == 'R');  
  CU_ASSERT(g.m[2][2] == 'R');  


  empty(&g);

 
  /* cas ou coord ne contient que des 'n' */

  coord.m[0][0] = 'n';  
  coord.m[0][1] = 'n';  
  coord.m[0][2] = 'n';  
  coord.m[1][0] = 'n';  
  coord.m[1][1] = 'n';  
  coord.m[1][2] = 'n';  
  coord.m[2][0] = 'n';  
  coord.m[2][1] = 'n';  
  coord.m[2][2] = 'n';  
   
  g = file_init("fichiers_test/test_changecolor1");

  CU_ASSERT(changeColor(&g,coord,'R') == 0); 
  CU_ASSERT(g.m[0][0] == 'B');  
  CU_ASSERT(g.m[0][1] == 'V');  
  CU_ASSERT(g.m[0][2] == 'R');  
  CU_ASSERT(g.m[1][0] == 'B');  
  CU_ASSERT(g.m[1][1] == 'B');  
  CU_ASSERT(g.m[1][2] == 'G');  
  CU_ASSERT(g.m[2][0] == 'M');  
  CU_ASSERT(g.m[2][1] == 'J');  
  CU_ASSERT(g.m[2][2] == 'B');  


  empty(&g);

  empty(&coord);

}







int main()
{
   CU_pSuite pSuite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry())
      return CU_get_error();

   /* add a suite to the registry */
   pSuite = CU_add_suite("Suite_1", NULL, NULL);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */
   if (NULL == CU_add_test(pSuite, "test of init_grid", test_init_grid)
    || NULL == CU_add_test(pSuite, "test of rand_init", test_rand_init)
    || NULL == CU_add_test(pSuite, "test of setColorAt", test_setColorAt)
    || NULL == CU_add_test(pSuite, "test of checkForWin", test_checkForWin)
    || NULL == CU_add_test(pSuite, "test of changeColor", test_changeColor)
    || NULL == CU_add_test(pSuite, "test of find4Connexe", test_find4Connexe)
)


   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
