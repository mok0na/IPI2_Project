#ifndef SOLVER_H
#define SOLVER_H

#include "stack.h"
#include "grid.h"

/* Solveur */
void solver(grid g, grid coord, stack solution, stack * solutionOpti, int coups);

/* Solveur heuristique */
stack solver2(grid g, int steps);
char bestStep(grid g);

/* Test du solveur */
int testSolver1(grid g);
int testSolver2(grid g);

#endif
