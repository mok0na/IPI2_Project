#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include "grid.h"

/**
 *\file grid.c
 *\brief Fonction de grid
 *\author ...
 *\version 0.1
 *\date
 *
 */


/**
 *\brief   Création d'une grille vide
 *\details   Dans le cas n<=0, la grille est initialisée à NULL et à une taille nulle, avec un message d'erreur
 *\param   n    entier représentant la taille de la grille. n>0
 *\return  g    une grille vide
 */
grid init_grid(int n)
{
    grid g;
    if (n>0)
    {
        g.size = n;

        g.m = (char **)malloc(n*sizeof(char*));
        int i;
        for (i=0; i<n; i++)
            g.m[i] = (char *)malloc(n*sizeof(char));
    }
    else
    {
            g.size = 0;
            g.m = NULL;
            fprintf(stderr,"Error: n<=0\n");    
    }

    return g;
}


/**
 *\brief  Création d'une grille aléatoire de 6 couleurs
 *\details Cette grille contiendra les couleurs 'B', 'V', 'R', 'J', 'M' et 'G'
 *\param  n   entier représentant la taille de la grille, n>0.
 *\return g   grille de taille n*n contenant des couleurs aleatoires
 */
grid rand_init(int n)
{
    grid g;
    g = init_grid(n);

    srand(time(NULL));
    int i, j;
    for (i=0; i<n; i++)
        for (j=0; j<n; j++)
        {
            /* Géneration d'une couleur aléatoire */
            switch(rand()%6 +1)
            {
                case 1: g.m[i][j] = 'B'; break;
                case 2: g.m[i][j] = 'V'; break;
                case 3: g.m[i][j] = 'R'; break;
                case 4: g.m[i][j] = 'J'; break;
                case 5: g.m[i][j] = 'M'; break;
                case 6: g.m[i][j] = 'G'; break;
            }
        }
    return g;
}


/**
 *\brief Création d'un fichier contenant les valeurs de test
 *\details Vérifie si le fichier est "bien formé" pour créer la grille correspondante
 *\param buf    char* contenant les valeurs à ajouter dans le fichiers
 *\param f      char*, nom du fichier à créer
 *\return       0 si le fichier est créé avec succès et 1 si une erreur s'est produite 
 */
int create_file(char* buf, char* f)
{
    /* Création et ouverture d'un fichier vide f, en mode écriture, si le fichier existe, écrase le fichier existant */
    FILE * fd = fopen(f, "w");

    int status = fwrite(buf, sizeof(char), strlen(buf), fd);
    if (status == 0) 
    {
        fprintf(stderr, "Fatal: erreur lors de la lecture de %s\n", f);
        return 1 ;
    }

    fclose(fd);
    return 0;
}

/**
 *\brief   Sauvegarde l'état de la grille dans un fichier
 *\param    g   la grille à sauvegarder
 *\return   1 si la grille a étée sauvegardée et 0 sinon
*/
int file_of_grid(grid g)
{
    char buf[g.size*(g.size+1)+1];
    int i,j,k=0;
    for (i=0; i<g.size; i++)
    {
        for (j=0; j<g.size; j++)
        {
            buf[k] = g.m[i][j];
            k++;
        }
        buf[k] = '\n';
        k++;
    }
    buf[k] = '\0';
    return (1-create_file(buf, "./saves/deoxys.cf"));
}


/**
 *\brief   Sauvegarde le nombre de coups restants pour la partie arrêtée dans un fichier
 *\param    steps le nombre de coups à sauvegarder
 *\return   1 si le nombre a été sauvegardé et 0 sinon
*/
int file_of_steps(int step)
{
    char buf[5];
    sprintf(buf, "%i", step);
    return (1-create_file(buf, "./saves/missingNo.cf"));
}


/**
 *\brief   Récupère le nombre de coups depuis un fichier
 *\param    f le nom du fichier contenant le nombre
 *\return   le nombre de coups contenu dans f
*/
int step_init(char * f)
{
    /* Ouverture du fichier en mode lecture */
    FILE * fd = fopen(f, "r");
    if (fd == NULL) {
        fprintf(stderr, "%s:Fatal: erreur de ouverture du fichier %s: %s\n", f, f, strerror(errno));
    }

    /* Calcul de la taille du fichier */    
    int status = fseek(fd, 0, SEEK_END);
    if (status == -1)
    {
        fprintf(stderr, "%s:Fatal: Erreur lors du deplacement du curseur dans %s\n", f, f);
    }
    int fsize = ftell(fd);

    /* Lecture du fichier dans le buffer */
    char * buf = malloc(fsize*sizeof(char));
    fseek(fd, 0, SEEK_SET);
    status = fread(buf, sizeof(char), fsize, fd);
    if (status == 0)
    {
        fprintf(stderr, "%s:Fatal: erreur lors de la lecture de %s\n", f, f);
    }

    int step = atoi(buf);

    fclose(fd);
    free(buf);
    return step;
}


/**
 *\brief   Création d'une grille à partir d'un fichier
 *\details Vérifie si le fichier est "bien formé" pour créer la grille correspondante
 *\param   f   un nom de fichier
 *\return  g   grille obtenu à partir de f si le fichier est correct et une grille NULL de taille 0 s'il est malformé
 */
grid file_init(char * f)
{
    int error = 0;

    /* Ouverture du fichier en mode lecture */
    FILE * fd = fopen(f, "r");
    if (fd == NULL) {
        fprintf(stderr, "%s:Fatal: erreur de ouverture du fichier %s: %s\n", f, f, strerror(errno));
        error = 1;
    }

    /* Calcul de la taille du fichier */    
    int status = fseek(fd, 0, SEEK_END);
    if (status == -1)
    {
        fprintf(stderr, "%s:Fatal: Erreur lors du deplacement du curseur dans %s\n", f, f);
        error = 1;
    }
    int fsize = ftell(fd);

    /* Lecture du fichier dans le buffer */
    char * buf = malloc(fsize*sizeof(char));
    fseek(fd, 0, SEEK_SET);
    status = fread(buf, sizeof(char), fsize, fd);
    if (status == 0)
    {
        fprintf(stderr, "%s:Fatal: erreur lors de la lecture de %s\n", f, f);
        error = 1;
    }

    /* Calcul de la taille que la grille doit avoir */
    int i = 0;
    int n = 0;
    while (i<fsize && buf[i]!='\n')
    {
        if (buf[i]!=' ')
            n++;
        i++;
    }

    /* Vérification de la validité du fichier */
    int j, k;
    i = 0; j = 0; k = 0;
    while (k<fsize)
    {
        if (buf[k] == '\n') { i++; j = 0; }
        else
        {
            if (buf[k] != ' ')
            {
                if (buf[k]=='B' || buf[k]=='V' || buf[k]=='R' || buf[k]=='J' || buf[k]=='M' || buf[k]=='G') { j++; }
                else { error = 1; break;}
            }
        }
        if (j>n) {error = 1;break;}
        k++;
    }
    if (buf[fsize-1] != '\n') i++; 
    if (i>n) error = 1;

    /* Initialisation d'une grille vide de taille n*n */
    grid g;
    if (!error)
    {
        g = init_grid(n);
        /* Remplissage de la grille avec les valeurs contenues dans le fichier si pas d'erreur */
        i = 0; j=0; k=0;
        while (k<fsize)
        {
            if (buf[k]!=' ')
            {
                if (buf[k]!='\n') { g.m[i][j] = buf[k]; j++; }
                else { i++; j=0; }
            }
            k++;
        }
    }
    else
    {
        fprintf(stderr, "Fichier invalide\n");
        g = init_grid(-1);
    }

    fclose(fd);
    free(buf);
    return g;
}


/**
 *\brief Libération de l'espace mémoire occupée par la grille
 *\param  g   pointeur vers une grille
 *\return  
 */
void empty(grid * g)
{
    int i;
    for (i=0; i<g->size; i++)
        free(g->m[i]);
    free(g->m);
}


/**
 *\brief Affichage de la grille
 *\param g  une grille valide
 */
void printGrid(grid g)
{
    int i, j;
    for (i=0; i<g.size; i++)
    {
        for (j=0; j<g.size-1; j++)
	    printf("%c ", g.m[i][j]);
        printf("%c\n", g.m[i][g.size-1]);
    }
}


/**
 *\brief Remplacement de la couleur de la case (x,y) par c
 *\param g   pointeur vers une grille
 *\param x   l'abscisse du point à modifier
 *\param y   l'ordonné du point à modifier
 *\param c   une couleur, de type char
 *\return   0 si la couleur dans la case a bien été remplacée et 1 sinon
 * remplace la couleur de la case (x, y) par la couleur c
 */
int setColorAt(int x, int y, char c, grid* g)
{
    if( x >= g->size || y>= g->size){
        fprintf(stderr, "setColorAt: x>=n ou y >= n\n");
        return 2;
    }

    if (c != 'B' && 
        c != 'V' &&
        c != 'R' &&
        c != 'J' &&
        c != 'M' &&
        c != 'G' &&
        c != 'n' &&
        c != 'y' &&
        c != 'c'){
        fprintf(stderr, "setColorAt: couleur non valide\n");
        return 1;
    }

    else{
        g->m[x][y] = c;
        return 0;
    }
}


/**
 *\brief Test de fin de jeu
 *\details  Si la grille est de taille nulle, on considere que la partie est gagnee (Ce cas n'est pas sensé arriver lors de l'execution du jeu)
 *\param   g    une grille valide
 *\return 1 si toutes les cases de la même couleur, 0 sinon
 */
int checkForWin(grid g)
{
    if (g.size != 0){

        char color = g.m[0][0];
        int i, j;
        for (i=0; i<g.size; i++)
            for (j=0; j<g.size; j++)
                if (g.m[i][j] != color)
                    return 0;
    return 1;
    }
    else{
        fprintf(stderr, "checkForWin: la grille est vide\n"); 
        return 2;
    }
}




/**
 *\brief Recherche des éléments 4-connexes
 *\details 
 *\param g           g une grille de jeu
 *\param coord       coord une grille pour stocker les cases à changer
 *\param i           i coordonnée x de la case à tester
 *\param j           i coordonnée y de la case à tester
 *\param colorNew    colorNew la couleur choisie par l'utilisateur
 *\return toujours 1
*/
int find4Connexe(grid g, grid * coord, int i, int j,char colorNew)
{
    /* première case toujours vraie */
    if (i==0 && j==0 && (*coord).m[0][0]!='y')
    {
        setColorAt(0,0,'y',coord);
        find4Connexe(g,coord,i+1,j,colorNew);
        find4Connexe(g,coord,i,j+1,colorNew);
    }
    if (i>=0 && i<g.size && j>=0 && j<g.size)
    {
        /* cas où la case n'a pas été vérifiée */
        if ((*coord).m[i][j]=='n')
	    {
	        /* cas où la case est bien de la couleur de la case (0,0) */
	        if (g.m[i][j]== g.m[0][0])
	        {
    	        setColorAt(i,j,'y',coord);
	            if ( i<g.size-1)
    		        find4Connexe(g,coord,i+1,j,colorNew); /*up*/
	            if ( j<g.size-1)
    		        find4Connexe(g,coord,i,j+1,colorNew); /*right*/
	            if ( i>0)
    		        find4Connexe(g,coord,i-1,j,colorNew); /*down*/
	            if ( j>0)
    		        find4Connexe(g,coord,i,j-1,colorNew); /*left*/
	        }
	        else
	        {
		  setColorAt(i,j,'c',coord);
	        }
	    }
    }
  return 1;
}
   

/**
 *\brief Change la couleur des cases 4-connexes
 *\param   g une grille qu'on doit changer de couleur
 *\param colorNew   colorNew la couleur choisie par l'utilisateur
 * \return 1 si une erreur s'est produite et 0 si tout s'est bien passée
*/
int changeColor(grid * g,grid coord, char colorNew)
{
  int i,j;
  for (i=0; i<g->size; i++)
    for (j=0; j<g->size; j++)
      {
	if (coord.m[i][j]=='y'){
	  if (setColorAt(i,j,colorNew,g) !=0){
            fprintf(stderr,"changeColor: probleme dans le setColorAt");
            return 1;
          }
        }
      }
  return 0;
}


/**
 *\brief   Remet toutes les cases de la grille servant à identifier la composante connexe à "not checked"
 *\param    g   la grille
*/
void init_coord(grid* g)
{
  int i,j;
  for (i=0; i<g->size; i++)
    for (j=0; j<g->size; j++)
      setColorAt(i,j,'n',g);
}

/**
 *\brief   Change la couleur de la composante connexe
 *\param    g   la grille à sauvegarder
 *\param    coord la grille servant à identifier la composante connexe
 *\param    colorNew    la couleur en laquelle il faut colorier la composante
*/
void spreadColor(grid *g, grid * coord, char colorNew)
{
  init_coord(coord);
  find4Connexe(*g, coord, 0, 0, colorNew);
  changeColor(g,*coord,colorNew);
}

/**
 *\brief Vérifie si 2 grilles sont égales
 *\param g1 une grille
 *\param g2 une seconde grille
 * \return 1 si g1 et g2 sont égales et 0 si elles sont différentes
*/
int gridEquals(grid g1, grid g2)
{
  
  if (g1.size == g2.size)
  {
    if (g1.size != 0)
    {
        int i, j;
        for (i=0; i<g1.size; i++)
            for (j=0; j<g1.size; j++)
                if (g1.m[i][j] != g2.m[i][j])
                    return 0;
        return 1;
    }
    else return 1;
    
  }
  else
    return 0;
}

/**
 *\brief Copie une grille dans une autre
 *\param old    la grille à copier
 *\param new    la grille dans laquelle on va la recopier
*/
void gridCopy(grid old,grid *new)
{
  int i, j;
  for (i=0; i<old.size; i++)
    for (j=0; j<old.size; j++)
      new->m[i][j]=old.m[i][j];
}


/**
 *\brief Change la couleur des cases 4-connexes
 *\param coord la grille servant à identifier la composante connexe
 * \return la taille de la composante connexe
*/
int size4Connexe(grid coord)
{
    int sz = 0;
    int i,j;
    for (i=0; i<coord.size; i++)
        for (j=0; j<coord.size; j++)
            if (coord.m[i][j] == 'y')
                sz++;
    return sz;
}
