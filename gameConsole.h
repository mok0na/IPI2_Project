#ifndef GAMECONSOLE_H
#define GAMECONSOLE_H

#include "grid.h"

/* Fonctions utilisées pour la version console */
void header();
int chooseSize();
char choice();
void saveGame(grid g, int step);
void hint(grid g);
void finishGame(grid * g, int step);

#endif
