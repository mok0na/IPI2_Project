\contentsline {chapter}{\numberline {1}R\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}D\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Data Structure Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Data Structures}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}File Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}File List}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}Data Structure Documentation}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}grid Struct Reference}{7}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Detailed Description}{7}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Field Documentation}{7}{subsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.2.1}m}{7}{subsubsection.4.1.2.1}
\contentsline {subsubsection}{\numberline {4.1.2.2}size}{7}{subsubsection.4.1.2.2}
\contentsline {chapter}{\numberline {5}File Documentation}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}grid.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}c File Reference}{9}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Detailed Description}{10}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Function Documentation}{10}{subsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.2.1}change\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Color}{10}{subsubsection.5.1.2.1}
\contentsline {subsubsection}{\numberline {5.1.2.2}check\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Case}{11}{subsubsection.5.1.2.2}
\contentsline {subsubsection}{\numberline {5.1.2.3}check\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}For\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Win}{11}{subsubsection.5.1.2.3}
\contentsline {subsubsection}{\numberline {5.1.2.4}empty}{12}{subsubsection.5.1.2.4}
\contentsline {subsubsection}{\numberline {5.1.2.5}file\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}init}{13}{subsubsection.5.1.2.5}
\contentsline {subsubsection}{\numberline {5.1.2.6}find4\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Connexe}{14}{subsubsection.5.1.2.6}
\contentsline {subsubsection}{\numberline {5.1.2.7}init\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}grid}{14}{subsubsection.5.1.2.7}
\contentsline {subsubsection}{\numberline {5.1.2.8}print\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Grid}{15}{subsubsection.5.1.2.8}
\contentsline {subsubsection}{\numberline {5.1.2.9}rand\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}init}{15}{subsubsection.5.1.2.9}
\contentsline {subsubsection}{\numberline {5.1.2.10}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Color\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}At}{16}{subsubsection.5.1.2.10}
\contentsline {section}{\numberline {5.2}grid.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{17}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Typedef Documentation}{18}{subsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.1.1}grid}{18}{subsubsection.5.2.1.1}
\contentsline {subsection}{\numberline {5.2.2}Function Documentation}{18}{subsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.1}change\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Color}{18}{subsubsection.5.2.2.1}
\contentsline {subsubsection}{\numberline {5.2.2.2}check\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Case}{19}{subsubsection.5.2.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.3}check\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}For\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Win}{19}{subsubsection.5.2.2.3}
\contentsline {subsubsection}{\numberline {5.2.2.4}empty}{20}{subsubsection.5.2.2.4}
\contentsline {subsubsection}{\numberline {5.2.2.5}file\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}init}{21}{subsubsection.5.2.2.5}
\contentsline {subsubsection}{\numberline {5.2.2.6}find4\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Connexe}{22}{subsubsection.5.2.2.6}
\contentsline {subsubsection}{\numberline {5.2.2.7}init\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}grid}{22}{subsubsection.5.2.2.7}
\contentsline {subsubsection}{\numberline {5.2.2.8}print\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Grid}{23}{subsubsection.5.2.2.8}
\contentsline {subsubsection}{\numberline {5.2.2.9}rand\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}init}{23}{subsubsection.5.2.2.9}
\contentsline {subsubsection}{\numberline {5.2.2.10}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Color\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}At}{24}{subsubsection.5.2.2.10}
\contentsline {section}{\numberline {5.3}main.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}c File Reference}{25}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Function Documentation}{25}{subsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.1.1}main}{25}{subsubsection.5.3.1.1}
\contentsline {section}{\numberline {5.4}R\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}D\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}md File Reference}{26}{section.5.4}
