#ifndef GRID_H
#define GRID_H


/* Structure de la grille */
typedef struct grid grid;

struct grid
{
    int size;
    char **m;
};

/* Fonctions sur l'initialisation de la grille */
grid init_grid(int n);
grid rand_init(int n);
int create_file(char* buf, char* f);
grid file_init(char * f);

/* Fichiers de sauvegarde */
int file_of_steps(int step);
int file_of_grid(grid g);
int step_init(char * f);

/* Libération de l'espace mémoire */
void empty(grid *g);

/* Affichage de la grille */
void printGrid(grid g);

/* Changement de couleur de la case (x,y) */
int setColorAt(int x, int y, char c, grid* g);

/* Test de fin de jeu */
int checkForWin(grid g);

int find4Connexe(grid g, grid * coord, int i, int j, char colorNew);
int changeColor(grid * g,grid coord, char colorNew);
void spreadColor(grid *g,grid * coord, char colorNew);

/* Teste si deux grilles sont identiques */
int gridEquals(grid g1, grid g2);

/* Copie d'une grille */
void gridCopy(grid old,grid *new);

/* Initialisation de coord avec que des 'n' */
void init_coord(grid* coord);

/* Calcul de la taille de la composante 4 connexe */
int size4Connexe(grid coord);

#endif 
