Projet IPI2 - ColorFlood
=========================
    
Installation du jeu
--------------------

Vous pouvez installer ColorFlood de deux manière différentes :
* Avec le fichier Makefile
* En compilant manuellement les fichiers de code
   
### Installation avec le Makefile

Avant d'installer le jeu, veillez à installer au préalable les packages **libsdl2-dev**, **libsdl2-image-dev** et **libsdl2-ttf-dev**
Lancez simplement la commande suivante dans un terminal :
```
make
```
   
### Installation manuelle

Compilez dans un premier temps les fichiers de code .c en fichiers .o avec la commande :
```
gcc *.c -o *.o
```

Compilez ensuite les fichiers en .o en un executable avec la commande :
```
gcc *.o -o ColorFlood
```

   
Lancement du jeu
-----------------

Démarrez le jeu après l'avoir installé avec la commande (en remplaçant 'n' par un nombre entier) :
```
./ColorFlood n
```

**************************

Déroulement d'une partie
-------------------------

### But du jeu

L'objectif est d'étendre la tache de couleur commençant en haut à gauche jusqu'à ce qu"elle recouvre l'intégralité de la grille. 

### Début de la partie

Au démarrage, choisissez dans un premier temps la taille de la grille sur laquelle vous souhaitez jouer en cliquant sur le bouton correspondant.
Choisissez ensuite le nombre de coups maximum autorisé pour la partie.

### Déroulement d'un tour

A chaque tour, vous devez selectionner une couleur en cliquant sur une case de cette couleur. Ceci aura pour effet de changer la couleur de la tache en haut à gauche,
puisqu'elle comprendra à présent les cases qui lui étaient adjacentes de la couleur sélectionée. Il faut ainsi augmenter la taille de la tache jusqu'à ce qu'elle recouvre
l'intégralité de la grille et avant d'avoir épuisé votre réserve de coups.


Commandes
----------

Appuyez sur [h] en cours de partie pour recevoir un indice sur le prochain coup à jouer.
Appuyez sur [esc] pour quitter la partie. Elle sera sauvegardée pour la terminer une prochaine fois.

Le solveur
-----------

### Terminer la partie

Si vous ne souhaitez pas finir votre partie, cliquez sur le bouton "Terminer la partie". Le solveur finira alors la partie à votre place en un nombre minimum de coups.

### Aide

Si vous vous retrouvez bloqué ou ne savez pas quel coup jouer pour gagner, cliquez sur le bouton "Aide". Il vous sera alors indiqué quelle couleur choisir pour avancer vers
la solution optimale (nombre de coups minimal).

**************************
   
Generation de la documentation Doxygen
---------------------------------------
  
Pour générer la documentation il vous faut avoir installé au préalable le logiciel **Doxygen**. Il vous suffira de lancer la commande 
d'installation ```make docs``` pour générer la documentation. La documention se trouve ensuite dans le dossier ```./Doxygen/html```. Ouvrez
le fichier **index.html** dans un navigateur et parcourez la documentation dans celui-ci. 
Pour ouvrir le fichier PDF de la documentation : 
```
evince ./Doxygen/latex/refman.pdf &
```


Format des fichiers de sauvegarde
----------------------------------

Vous pouvez commencer une partie à partir d'une grille prédéterminée. Pour cela vous devez créer un fichier texte respectant la forme :
* n lignes de n caractères
* les caractères peuvent être 'B', 'V', 'R', 'J', 'M', 'G'

   
**************************
   
Travailler sur le projet
-------------------------

### Pour installer le projet
```
git clone git@git.iiens.net:SRSTVTTAP/IPI2_Project.git
```
### Avant de commencer à travailler
``` 
$ git pull
```

### Pour uploads les changements
```
1. git status (pour voir ce qui a été changé)
2. git commit -a -m "décrire les changements"
3. git push (upload fichier)
```
