#include <stdio.h>
#include <unistd.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>

#include "constant.h"
#include "grid.h"
#include "game.h"
#include "SDL.h"
#include "solver.h"
#include "stack.h"



/* ================================
 * Mode SDL
 * ================================
 */

/**
 *\brief Sauvegarde la grille de jeu
 *\param    g   la grille
 *\param    step    
 *\return 1 si succès et 0 sinon
*/
int saveGameSDL(grid g, int step)
{
    if (file_of_grid(g))
        if (file_of_steps(step))
            return 1;
    return 0;
}


void hint(SDL_Surface* ecran, grid grid)
{
    char hint;
    hint = bestStep(grid);
    printf("%c", hint);
    int r, g, b;
    toRGB2(&r, &g, &b, hint);
    drawRectangle(ecran, 156, 44, 30, r, g, b);
}


int play(SDL_Surface* ecran, int size)
{
  SDL_Surface * nbCoups = NULL, * bar = NULL;
  SDL_Event event;
  SDL_Rect positionText;
  SDL_Rect positionBar;
  int x,y;
  int bpp;

  positionText.x = 5;
  positionText.y = 5;

  int continuer = 1, nb_coup = 0, nb_coup_max;
  switch(size)
  {
      case 12: nb_coup_max = 20; break;
      case 18: nb_coup_max = 30; break;
      case 24: nb_coup_max = 42; break;
      default: nb_coup_max = 30; break;
  }

  bar = IMG_Load("img/bar.jpg");
  positionBar.x = 0;
  positionBar.y = 0;

  /* Initialisation de sdl_ttf */
  TTF_Init();
  if(TTF_Init() == -1)
  {
      fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
      exit(EXIT_FAILURE);
  }

  /* Chargement d'une police */
  TTF_Font *police = NULL;
  police = TTF_OpenFont("arial.ttf", 35);

  SDL_Color white = {.r = 72, .g = 74, .b = 88};
  char nb_coups[20] = "";
  /* initialisation de coord*/
  grid coord = init_grid(size);
  init_coord(&coord);

  /* Initialisation d'une grille aléatoire de 6 couleur */
  grid g = rand_init(size);
  //fillScreen(ecran,0,0,0);
  drawGrid(ecran, g, (SCREEN_WIDTH-100)/(g.size));

  SDL_BlitSurface(bar, NULL, ecran, &positionBar);
  SDL_Flip(ecran);

  drawRectangle(ecran, 660, 1, 80, 255, 255, 255);

  while (continuer){
      SDL_WaitEvent(&event);
      switch(event.type) {
          case SDL_QUIT:
              continuer = 0;
              if (!checkForWin(g)) saveGameSDL(g, nb_coup);
              break;
          
          case SDL_KEYDOWN:
              switch (event.key.keysym.sym) {
                  case SDLK_m:
                      saveGameSDL(g, nb_coup); return 0;
                  case SDLK_h: 
                     hint(ecran, g);break;
		  case SDLK_ESCAPE:
                      if (!checkForWin(g)) saveGameSDL(g, nb_coup);
        	      continuer = 0;
                      break;
                  default: break;
              }
              break;

          case  SDL_MOUSEBUTTONDOWN:
              if (event.button.button == SDL_BUTTON_LEFT)
              {
                  //if (checkClick(event.button, 50, 100, 650, 650)) 
                  //{

                      x = event.button.x ;
                      y = event.button.y ;
                      bpp = ecran->format->BytesPerPixel;
                      /* Here p is the address to the pixel we want to retrieve */
                      Uint8 *p = (Uint8 *)ecran->pixels + y*ecran->pitch + x*bpp;
                      /* p[2]=R; p[1]=G; p[0]=B; */
                      char c = toChar(p[2], p[1], p[0]);
                      if ((c=='B' || c=='V' || c=='R' || c=='J' || c=='M' || c=='G') && c!=g.m[0][0])
                      {
                          drawRectangle(ecran, 156, 44, 30, 255, 255, 255);
                          nb_coup++;
                          spreadColor(&g,&coord,c);
                          updateGrid(ecran, coord, g, (SCREEN_WIDTH-100)/(g.size));
                
                          sprintf(nb_coups, " %i / %i", nb_coup, nb_coup_max);
                          nbCoups = TTF_RenderText_Blended(police,nb_coups, white);
                          if (checkForWin(g) && nb_coup<=nb_coup_max)
                          {
                              nbCoups = TTF_RenderText_Blended(police,"You win !!!", white);
                             positionText.x=300;
                             positionText.y=300;
                          }
                          else if (nb_coup>nb_coup_max)
                             {
                                  nbCoups = TTF_RenderText_Blended(police,"You lose", white);
                                  positionText.x=400;
                                  positionText.y=10;
                             }

                          drawRectangle(ecran, 5, 5, 35, 255, 255, 255);
                          drawRectangle(ecran, 40, 5, 35, 255, 255, 255);
                          drawRectangle(ecran, 75, 5, 35, 255, 255, 255);
                          SDL_BlitSurface(nbCoups, NULL, ecran, &positionText);
                          SDL_Flip(ecran);
                      }
                 // } else
                 if (checkClick(event.button, 12, 30, 100-12, 77-30))
                 {
                     /* Hint */
                     hint(ecran, g);
                 } else
                 if (checkClick(event.button, 550, 36, 644-550, 70-36))
                 {
                     /* Back to menu */
                     saveGameSDL(g, nb_coup);
        	     continuer = 0;
                 }

             }
             break;
        }
    }
    empty(&coord);
    empty(&g);
    TTF_CloseFont(police);
    TTF_Quit();

    return 0;
}



/**
 *\brief Jeu à partir du fichier de sauvegarde
 *\param    ecran   la SDL_surface où est affichée la partie
 *\return 0 à la fin de la partie
*/
int savedGame(SDL_Surface* ecran)
{
  SDL_Surface * nbCoups, * bar;
  SDL_Event event;
  SDL_Rect positionText;
  SDL_Rect positionBar;
  int x,y;
  int bpp;

  positionText.x = 5;
  positionText.y = 5;

  bar = IMG_Load("img/bar.jpg");
  positionBar.x = 0;
  positionBar.y = 0;

  int continuer = 1;
  /* Initialisation de la grille de jeu à partir des fichiers de sauvegarde */
  grid g = file_init("./saves/deoxys.cf");
  int size = g.size;
  int nb_coup = step_init("./saves/missingNo.cf");
  int nb_coup_max;

  switch(size)
  {
      case 12: nb_coup_max = 20; break;
      case 18: nb_coup_max = 30; break;
      case 24: nb_coup_max = 42; break;
      default: nb_coup_max = 30; break;
  }

  /* Initialisation de sdl_ttf */
  TTF_Init();
  if(TTF_Init() == -1)
  {
      fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
      exit(EXIT_FAILURE);
  }

  /* Chargement d'une police */
  TTF_Font *police = NULL;
  police = TTF_OpenFont("arial.ttf", 35);

  SDL_Color white = {.r = 72, .g = 74, .b = 88};
  char nb_coups[20] = "";
  /* initialisation de coord*/
  grid coord = init_grid(size);
  init_coord(&coord);

  //fillScreen(ecran,0,0,0);
  drawGrid(ecran, g, (SCREEN_WIDTH-100)/(g.size));

  SDL_BlitSurface(bar, NULL, ecran, &positionBar);
  SDL_Flip(ecran);

  drawRectangle(ecran, 660, 1, 80, 255, 255, 255);

  while (continuer){
      SDL_WaitEvent(&event);
      switch(event.type) {
          case SDL_QUIT:
              if (!checkForWin(g)) saveGameSDL(g, nb_coup);
              continuer = 0;
              break;
          
          case SDL_KEYDOWN:
              switch (event.key.keysym.sym) {
                  case SDLK_h: 
                     hint(ecran, g);break;
                  case SDLK_m: saveGameSDL(g, nb_coup); return 0;
		  case SDLK_ESCAPE:
                      if (!checkForWin(g)) saveGameSDL(g, nb_coup);
        	      continuer = 0;
                      break;
                  default: break;
              }
              break;

          case  SDL_MOUSEBUTTONDOWN:
              if (event.button.button == SDL_BUTTON_LEFT) {
                  x = event.button.x ;
                  y = event.button.y ;
                  bpp = ecran->format->BytesPerPixel;
                  /* Here p is the address to the pixel we want to retrieve */
                  Uint8 *p = (Uint8 *)ecran->pixels + y*ecran->pitch + x*bpp;
                  /* p[2]=R; p[1]=G; p[0]=B; */
                  char c = toChar(p[2], p[1], p[0]);
                  if ((c=='B' || c=='V' || c=='R' || c=='J' || c=='M' || c=='G') && c!=g.m[0][0])
                  {
                      drawRectangle(ecran, 156, 44, 30, 255, 255, 255);
                      nb_coup++;
                      spreadColor(&g,&coord,c);
                      updateGrid(ecran, coord, g, (SCREEN_WIDTH-100)/(g.size));
            
                      sprintf(nb_coups, " %i / %i", nb_coup, nb_coup_max);
                      nbCoups = TTF_RenderText_Blended(police,nb_coups, white);
                      if (checkForWin(g) && nb_coup<=nb_coup_max)
                      {
                          nbCoups = TTF_RenderText_Blended(police,"You win !!!", white);
                          positionText.x=300;
                          positionText.y=300;
                      }
                      else if (nb_coup>nb_coup_max)
                          {
                              nbCoups = TTF_RenderText_Blended(police,"You lose", white);
                              positionText.x=400;
                              positionText.y=10;
                          }
                          drawRectangle(ecran, 5, 5, 35, 255, 255, 255);
                          drawRectangle(ecran, 40, 5, 35, 255, 255, 255);
                          drawRectangle(ecran, 75, 5, 35, 255, 255, 255);
                      SDL_BlitSurface(nbCoups, NULL, ecran, &positionText);
                      SDL_Flip(ecran);
                 }
             }
             break;
        }
    }
    empty(&coord);
    empty(&g);
    TTF_CloseFont(police);
    TTF_Quit();

    return 0;
}
