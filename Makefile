PROG=ColorFlood
OBJ=main.o grid.o stack.o solver.o tree.o gameConsole.o
OPTIONS=-Wall -Wextra

all: $(PROG) ColorFloodSDL clean

grid.o: grid.h
main.o: grid.h
stack.o: stack.h
tree.o: tree.h grid.h stack.h
solver.o: solver.h grid.h stack.h tree.h
gameConsole.o: gameConsole.h grid.h solver.h


%.o: %.c
	gcc -c $< $(OPTIONS)
$(PROG): $(OBJ)
	gcc -o $(PROG) $(OBJ)


mainSDL.o: constant.h grid.h SDL.h option.h game.h solver.h stack.h tree.h
SDL.o: SDL.h grid.h
option.o: option.h constant.h SDL.h game.h
game.o: game.h constant.h grid.h SDL.h
solver.o: solver.h grid.h stack.h tree.h

ColorFloodSDL: mainSDL.o grid.o SDL.o option.o game.o solver.o stack.o tree.o
	gcc -o ColorFloodSDL mainSDL.o grid.o SDL.o option.o game.o solver.o stack.o tree.o -lSDL -lSDL_ttf -lSDL_image


CUnit_tests: tests_cunit.o grid.o
	gcc tests_cunit.o grid.o -o CUnit_tests -lcunit

docs: 
	doxygen ./Doxyfile

clean:
	rm -f *.o
