#include <stdio.h>
#include <time.h>
#include "grid.h"
#include "stack.h"
#include "tree.h"


char colors[6] = {'B', 'V', 'R', 'J', 'M', 'G'};


/**
 *\brief   vérifie si la nouvelle solution est plus rapide que la meilleure jusqu'à présent
 *\param   solution     la solution dont on test l'optimalité
 *\param   solutionOpti la meilleure solution jusqu'à présent 
*/
void solutionFound(stack solution, stack * solutionOpti)
{
    if (is_empty(*solutionOpti))
        *solutionOpti = reverse(solution);
    else
        if (depth(solution)<depth(*solutionOpti)){
	    stack_empty(solutionOpti);
	    *solutionOpti = reverse(solution);
        }
}


/**
 *\brief   détermine par énumération exhaustive la meilleure solution pour résoudre la grille
 *\param   g            la grille de jeu à résoudre
 *\param   solution     la solution dont on test l'optimalité
 *\param   solutionOpti la meilleure solution jusqu'à présent
 *\param   coups        le nombre de coups restant pour pouvoir gagner
*/
void solver(grid g, grid coord, stack solution, stack *solutionOpti, int coups)
{
    int i;
    /* La grille coord2 est une copie de coord et permet de vérifier s'il y a propagation de la composante connexe */
    grid coord2=init_grid(g.size);
    /* On définit une grille g2 qui est une copie de g et on modifiera g2 par la suite dans la boucle for */
    grid g2 = init_grid(g.size);
    for (i=0; i<6; i++)
    {
        /* A chaque nouvelle itération, g2 est redéfinit et elle contient les mêmes couleurs que g */
        gridCopy(g,&g2);
        /* La couleur jouée doit être différente de la précédente */
        if (g.m[0][0]!=colors[i])
        {
	    init_coord(&coord2);
            /* Modification de coord2, qui sera la nouvelle composante connexe */
	    find4Connexe(g2, &coord2, 0, 0, colors[i]);
            /* On teste si la nouvelle composante connexe est identique à la précédente */
	    if (!gridEquals(coord,coord2))
	    {
                /* empile la couleur jouée */
	        push(colors[i], &solution);
                /* Modification de la grille g2 */
	        changeColor(&g2,coord2,colors[i]);
	        if (checkForWin(g2))
                {
                    solutionFound(solution, solutionOpti);
                }
                else
                    /* Si le nombre de coups égale celui de la solution sauvegardée alors on arrête */
                    if (coups+1 < depth(*solutionOpti)-1 || *solutionOpti==NULL)
                        solver(g2,coord2,solution,solutionOpti, coups+1);
                /* On revient à l'étape précédente en dépilant solution */
                pop(&solution);
            }
        }
    }
    /* Libération des espaces utilisées */
    empty(&coord2);
    empty(&g2);
}





tree treeSolver(grid g, int step, int sz)
{
    grid coord = init_grid(g.size);
    tree t = createTree(g, sz);

    int i, j=0;
    if (step <= 2)
    {
        for (i=0;i<6;i++)
        {
            if (g.m[0][0] != colors[i])
            {

                grid tmp = init_grid(g.size);
                gridCopy(g, &tmp);
                
                spreadColor(&tmp, &coord, colors[i]);

		sz = size4Connexe(coord);
                t->sons[j] = treeSolver(tmp, step+1, sz);
                j++;
            }
        }
    }
    empty(&coord);
    return t;
}




char bestStep (grid g)
{
    tree t = treeSolver(g, 0, 0);
    char best;
    int bestSize = t->sz;
    int i, j, k;
    tree son1,son2,son3;
    for (i=0; i<5; i++)
    {
        son1 = t->sons[i];
        if (checkForWin(son1->g))
            return son1->g.m[0][0];
        for (j=0; j<5; j++)
        {
            son2 = son1->sons[j];
            if (checkForWin(son2->g))
            {
                bestSize = son2->sz;
                best = son1->g.m[0][0];
            } else
            for (k=0; k<5; k++)
            {
                son3 = son2->sons[k];
                if (son3->sz > bestSize)
                {
                    bestSize = son3->sz;
                    best = son1->g.m[0][0];
                }
            }
        }
    }
    return best;
}


stack solver2(grid g){
    stack solutionHeuristic = NULL; 
    stack solutionFinale = NULL;
    char next;
    grid coord = init_grid(g.size);

    while (!checkForWin(g))
    {
        next = bestStep(g);
        push(next, &solutionHeuristic); 
        spreadColor(&g, &coord, next);
    }
        
    solutionFinale = reverse(solutionHeuristic);

    stack_empty(&solutionHeuristic);
    empty(&coord);

    return solutionFinale;
}


int testSolver1(grid g)
{
    printf("\nTest du solveur ====================");
    clock_t start, end;
    stack solutionOpti = NULL, solution = NULL;
    float seconds;
    grid coord = init_grid(g.size);

    printGrid(g);
    printf("\n");

    start = clock();

    solver(g,coord,solution,&solutionOpti,0);
    
    end = clock();
    
    seconds = (float)(end - start) / CLOCKS_PER_SEC;

    printf("Solution optimale : \n");
    stack_print(solutionOpti);

    printf("Temps écoulé : %fs\n", seconds);
    printf("=================== Fin test solveur\n");

    stack_empty(&solution);
    stack_empty(&solutionOpti);
    empty(&coord);

    return seconds;
}


int testSolver2(grid g)
{
    printf("\nTest du solveur heuristique ====================");
    clock_t start, end;
    float seconds;
    grid coord = init_grid(g.size);
    stack solutionHeuristic = NULL;


    printGrid(g);
    printf("\n");

    start = clock();

    solutionHeuristic = solver2(g);

    end = clock();
    seconds = (float)(end - start) / CLOCKS_PER_SEC;

    stack_print(solutionHeuristic);
    printf("Temps écoulé : %fs\n", seconds);
    printf("============================== Fin test solveur\n");

    stack_empty(&solutionHeuristic);
    empty(&coord);

    return seconds;
}
