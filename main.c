#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "grid.h"
#include "stack.h"
#include "solver.h"
#include "gameConsole.h"



int main()
{
    grid g;
    int size;
    int nb_coup, nb_coup_max;
    char c;
    grid coord;
    int play = 1;

    header();

    /* Vérifier si un fichier de sauvegarde existe, puis génération de la grille */
    if( access( "./saves/deoxys.cf", F_OK ) != -1 && access( "./saves/missingNo.cf", F_OK ) != -1) 
    {
        printf("Voulez-vous continuer la partie précédente ? (y/n) ");
        char cont;
        scanf("%c",&cont);
        getchar();
        /* Chargement du jeu et du nombre de coups déjà joués à partir des fichiers de sauvegarde */
        if (cont == 'y')
        {
            nb_coup = step_init("./saves/missingNo.cf");
            g = file_init("./saves/deoxys.cf");
            size = g.size;
        }
        /* Choix de la taille de la grille par l'utilisateur */
        else 
        {
            printf("\n");
            size = chooseSize();
            /* Initialisation d'une grille aléatoire de 6 couleur */
            g = rand_init(size);
            nb_coup = 0;
        }
    }
    /* Choix de la taille de la grille par l'utilisateur */
    else 
    {
        size = chooseSize();
        /* Initialisation d'une grille aléatoire de 6 couleur */
        g = rand_init(size);
        nb_coup = 0;
    }


    coord = init_grid(size); /* coord contient 'y' ou 'n' suivant si la couleur de la case doit être modifiée */


    /* Calcul du nombre de coups maximum en fonction de la taille de la grille */
    switch(size)
    {
      case 12: nb_coup_max = 20; break;
      case 18: nb_coup_max = 30; break;
      case 24: nb_coup_max = 42; break;
      default: nb_coup_max = 30; break;
    }
    

    /* Affichage de la grille */
    system("clear");
    printGrid(g);
    printf("\n");


    /* =================================
     * Test du solveur
     * =================================
     */

    /* Test du premier solveur */
    // testSolver1(g);
    /* Test très lent, ne pas essayer pour une grille de taille > 6 */

    /*test du solveur heuristique */
    // testSolver2(g);



    /* Boucle de jeu */
    while (play && !checkForWin(g))
    {
        c = choice();
        switch (c)
        {
            case 'f': finishGame(&g, nb_coup); play = 0; break;
            case 'h': hint(g); break;
            case 's': saveGame(g, nb_coup); break;
            case 'q': play = 0; printf("Vous avez quitté le jeu.\n"); break;
            default: if (c!=g.m[0][0])
                     {
                         /* Si la couleur choisie est différente de celle de la case en haut à gauche, alors on procède au changement de couleur */
                         nb_coup++;
                         spreadColor(&g,&coord,c);
                         if (nb_coup>=nb_coup_max)
	                     printf("Vous avez perdu, %d / %d coups\n", nb_coup,nb_coup_max);
	                 else
	                     printf("Vous avez joué %d / %d coups.\n", nb_coup,nb_coup_max);
                     }
                     break;
	}
        printf("\n");
        printGrid(g);
        printf("\n");
    }
    
    if (c!='q')
    {
        /* Victoire ? */
        if (nb_coup<nb_coup_max && checkForWin(g))
          printf("You win !\n");
        else
          printf("You lose, try again !\n");
    }

    /* Libération des espaces alloués */
    empty(&g);
    empty(&coord);

    return 0;
}
