#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>
#include <string.h>

#include "SDL.h"
#include "grid.h"


/**
 *\brief   Dessine un carré
 *\details  carré de largeur h et de couleur (r,g,b)
 *\param   ecran SDL_Surface sur laquelle est tracée le carré
 *\param   px   abcisse du pixel en haut à gauche du rectangle
 *\param   py   ordonnée du pixel en haut à gauche du rectangle
 *\param   size hauteur/largeur du carré
 *\param   r    composante rouge de la couleur du carré
 *\param   g    composante verte de la couleur du carré
 *\param   b    composante bleue de la couleur du carré
 */
void drawRectangle(SDL_Surface *ecran, int px, int py, int size, int r, int g, int b) {
    SDL_Rect rect;
    rect.x=px;
    rect.y=py;
    rect.h=rect.w=size;
    SDL_FillRect(ecran, &rect, SDL_MapRGB(ecran->format, r, g, b));
    SDL_Flip(ecran);
}


/**
 *\brief   Colorie toute la fenêtre
 *\details  avec la couleur (r,g,b)
 *\param   ecran SDL_Surface coloriée entièrement
 *\param   r    composante rouge de la couleur
 *\param   g    composante verte de la couleur
 *\param   b    composante bleue de la couleur
 */
void fillScreen(SDL_Surface *ecran, int r, int g, int b) {
    SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, r, g, b));
    SDL_Flip(ecran);
}

/**
 *\brief   Remplace les valeurs dans r,g et b par les composantes correspondantes pour la couleur de la case sélectionée
 *\param   *r    contiendra la composante rouge de la couleur de la case
 *\param   *g    contiendra la composante verte de la couleur de la case
 *\param   *b    contiendra la composante bleue de la couleur de la case
 *\param   grid  la grille de jeu
 *\param   x     abcisse de la case donc on va regarder la couleur
 *\param   y     ordonnée de la case donc on va regarder la couleur
 */
void toRGB(int* r, int* g, int* b, grid grid, int x, int y)
{
  switch(grid.m[x][y])
  {
  case 'B': {*r=155;*g=88; *b=181; break;}
  case 'V': {*r=79; *g=185; *b=243; break;}
  case 'R': {*r=238; *g=238; *b=238; break;}
  case 'J': {*r=255; *g=201; *b=5; break;}
  case 'M': {*r=255; *g=131; *b=67; break;}
  case 'G': {*r=240; *g=61; *b=91; break;}
  }
}

/**
 *\brief   Remplace les valeurs dans r,g et b par les composantes correspondantes pour la couleur c
 *\param   *r    contiendra la composante rouge de la couleur de la case
 *\param   *g    contiendra la composante verte de la couleur de la case
 *\param   *b    contiendra la composante bleue de la couleur de la case
 *\param   c     une couleur
 */
void toRGB2(int* r, int* g, int* b, char c)
{
  switch(c)
  {
  case 'B': {*r=155;*g=88; *b=181; break;}
  case 'V': {*r=79; *g=185; *b=243; break;}
  case 'R': {*r=238; *g=238; *b=238; break;}
  case 'J': {*r=255; *g=201; *b=5; break;}
  case 'M': {*r=255; *g=131; *b=67; break;}
  case 'G': {*r=240; *g=61; *b=91; break;}
  }
}
/**
 *\brief Renvoit la couleur correspondant à un code RGB
 *\param    r   Composante rouge
 *\param    g   Composante verte
 *\param    b   Composante bleue
 *\return le char correspondant à la couleur, ou 'n' si la couleur n'est pas correct
*/
char toChar (int r, int g, int b)
{
  if (r==155 && g==88 && b==181)
    return 'B';
  if (r==79 && g==185 && b==243)
    return 'V';
  if (r==238 && g==238 && b==238)
    return 'R';
  if (r==255 && g==201 && b==5)
    return 'J';
  if (r==255 && g==131 && b==67)
    return 'M';
  if (r==240 && g==61 && b==91)
    return 'G';
  return 'n';
}

/**
 *\brief Dessine la grille
 *\param    ecran   SDL_Surface sur laquelle sera dessinée la grille
 *\param    grid    La grille de jeu contenant l'état de la partie
 *\param    size    La taille du carré
*/
void drawGrid(SDL_Surface *ecran, grid grid, int size)
{
  int r, g, b;
  int i, n;
  int offset_x=50, offset_y=100;
  for (n=0; n<grid.size; n++)
    for (i=n; i>=0; i--)
    {
      toRGB(&r, &g, &b, grid, i, n-i);
      drawRectangle(ecran, offset_x+i*size, offset_y+(n-i)*size, size, r, g, b);
    }
  for (n=grid.size; n<2*grid.size-1; n++)
    for (i=grid.size-1; i>n-grid.size; i--)
    {
      toRGB(&r, &g, &b, grid, i, n-i);
      drawRectangle(ecran, offset_x+i*size, offset_y+(n-i)*size, size, r, g, b);
    }
}

/**
 *\brief    Actualise la grille
 *\details  Ne redessine que les carrés qui changent de couleur
 *\param    ecran   SDL_Surface sur laquelle sera dessinée la grille
 *\param    grid    La grille de jeu contenant l'état de la partie
 *\param    size    La taille du carré
*/
void updateGrid(SDL_Surface *ecran, grid coord, grid grid, int size)
{
  int r, g, b;
  int i, n;
  int offset_x=50, offset_y=100;
  for (n=0; n<grid.size; n++)
    for (i=n; i>=0; i--)
    {
      if (coord.m[i][n-i]=='y')
      {
        toRGB(&r, &g, &b, grid, i, n-i);
        drawRectangle(ecran, offset_x+i*size, offset_y+(n-i)*size, size, r, g, b);
      }
    }
  for (n=grid.size; n<2*grid.size-1; n++)
    for (i=grid.size-1; i>n-grid.size; i--)
    { 
      if (coord.m[i][n-i]=='y')
      {
        toRGB(&r, &g, &b, grid, i, n-i);
        drawRectangle(ecran, offset_x+i*size, offset_y+(n-i)*size, size, r, g, b);
      }

    }
}


/**
 *\brief    Vérifie que le clic est bien dans la surface de jeu
 *\param    event   Le MouseButtonEvent du clic
 *\param    x       abcisse de la gauche de la surface
 *\param    hx      abcisse de la droite de la surface
 *\param    y       ordonnée du bas de la surface
 *\param    hy      ordonnée du haut de la surface
 *\return   1 si le clic est dedans et 0 si il est dehors
*/
int checkClick(SDL_MouseButtonEvent event,int x,int hx,int y,int hy)
{
  if (event.x>x && event.x< hx)
    {
      if (event.y>y && event.y< hy)
    	return 1;
      return 0;
    }
  return 0;
}
