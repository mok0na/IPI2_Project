#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>
#include <unistd.h>

#include "constant.h"
#include "SDL.h"
#include "game.h"
#include "option.h"

int main()
{
    SDL_Surface *ecran = NULL, *menu = NULL;
    SDL_Rect positionMenu;
    SDL_Event event;
    const SDL_VideoInfo* info = NULL;

    int continuer = 1;

    /* initialisation de la fenêtre d'affichage */
    if(SDL_Init( SDL_INIT_VIDEO ) < 0) {
        /* Failed, exit. */
        fprintf(stderr, "Video initialization failed: %s\n", SDL_GetError());
        SDL_Quit();
    }

    info = SDL_GetVideoInfo();
    if(!info) {
        /* This should probably never happen. */
        fprintf(stderr, "Video query failed: %s\n", SDL_GetError());
        SDL_Quit();
    }

    /* Taille de la fenetre */
    ecran = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_HWSURFACE);
    SDL_WM_SetCaption("Color Flood", NULL);

    menu = IMG_Load("img/background1.jpg");
    positionMenu.x = 0;
    positionMenu.y = 0;


    while (continuer)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = 0;
                break;

            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_q:
                 	continuer = 0;
                        break;
                    case SDLK_ESCAPE: // Veut arrêter le jeu
                        continuer = 0;
                        break;
                    case SDLK_d:
                    	play(ecran,12);
                    	break;

                    case SDLK_f:
                    	play(ecran,18);
                    	break;
                    case SDLK_g:
                    	play(ecran,24);
                    	break;
                    default: break;
                }
                break;

            case SDL_MOUSEBUTTONDOWN:
        	if(event.button.button == SDL_BUTTON_LEFT)
        	{
        	    /*quitter*/
          	    if (checkClick(event.button,50,210,180,300))
            		continuer = 0;
          		/*new game*/
          	    else if (checkClick(event.button,40,290,430,578))
          	         {
            		     option(ecran);
          		 }
          		 /*continue*/
          		 else if (checkClick(event.button,520,730,130,260))
          	              {
                                  if( access( "./saves/deoxys.cf", F_OK ) != -1 && access( "./saves/missingNo.cf", F_OK ) != -1) 
                                      savedGame(ecran);
          		      }
          		      /*help*/
          		      else if (checkClick(event.button,580,720,400,480))
            			       fillScreen(ecran,0,0,0);
        	}
        	break;
        }


        // Effacement de l'écran
        SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));
        SDL_BlitSurface(menu, NULL, ecran, &positionMenu);
        SDL_Flip(ecran);
    }

    SDL_FreeSurface(menu);
    SDL_Quit();

    return EXIT_SUCCESS;
}
