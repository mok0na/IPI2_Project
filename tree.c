#include <stdlib.h>
#include <stdio.h>
#include "stack.h"
#include "grid.h"
#include "tree.h"

/**
 *\brief   Création d'un arbre 
 *\details Il contient un noeud et 5 fils vides
 *\param    g       la grille à mettre dans le noeud
 *\param    sz      taille de la composante connexe de cette grille
 *\returns  l'arbre ainsi créé
 */
tree createTree(grid g, int sz)
{
    tree t = (tree)malloc(sizeof(node));
    t->g = g;
    t->sz = sz;
    int i;
    for (i=0; i<5; i++)
        t->sons[i] = NULL;
    return t;
}

/**
 *\brief   Vérifie si un arbre est vide
 *\param    t    l'arbre à'
 *\return   1 si t est vide et 0 sinon
*/
int isEmptyTree(tree t){
    return (t == NULL);
}


/**
 *\brief   Vérifie si un arbre a bien 5 fils
 *\param    t    l'arbre à vérifier
 *\return   1 si t a bien 5 fils et 0 sinon
*/
int hasSons(tree t)
{
    int i;
    for (i=0; i<5; i++)
    {
        if (t->sons[i] == NULL)
            return 0;
    }
    return 1;
}


/**
 *\brief   Vide l'espace mémoire occupé par un arbre
 *\details agit récursivement sur les sous-arbres
 *\param    g   l'arbre à détruire'
 */
void emptyTree(tree * t){
    int i;
    if (!hasSons(*t))
    {
        empty(&((*t)->g));
        free(t);
    }
    else
        for (i=0; i<5; i++)
            emptyTree(&((*t)->sons[i]));
}
