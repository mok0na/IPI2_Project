#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "gameConsole.h"
#include "grid.h"
#include "solver.h"

void header()
{
    system("clear");
    printf ("### ### #   ### ###  ### #   ### ### ##\n#   # # #   # # # #  #   #   # # # # # #\n#   # # #   # # ###  ##  #   # # # # # #\n#   # # #   # # ##   #   #   # # # # # #\n### ### ### ### # #  #   ### ### ### ##\n\n\n");

}


int chooseSize()
{
    int size;
    int d;
    printf("Veuillez choisir la taille de la grille : \n");
    printf("1 - 12x12\n2 - 18x18\n3 - 24x24\n");
    scanf("%d",&d);
    getchar();
    switch (d)
    {
      case 1: size = 12; break;
      case 2: size = 18; break;
      case 3: size = 24; break;
      default: size = 18; break;
    }
    return size;
}


char choice()
{
    char c;
    printf ("Veuillez choisir une couleur parmi 'B', 'V', 'R', 'J', 'M', 'G'. \n\n");
    printf("f - finir le jeu\nh - aide \ns - sauvegarder\nq - quitter\n\n");
    printf("Quel est votre choix ? \n");
    scanf("%s",&c);

    system("clear");

    printf("Vous avez saisi %c.\n\n", c);

    if (c!='B' && c!='V' && c!='R' && c!='J' && c!='M' && c!='G' && c!='h' && c!='s' && c!='q' && c!='f')
    {
        printf("Je n'ai pas compris votre choix. ");
        c = choice();
    }
    return c;
}


void saveGame(grid g, int step)
{
    if (file_of_grid(g))
        if (file_of_steps(step))
            printf("Votre partie est maintenant sauvegardé.\n\n");
        else
            printf("Erreur lors de la sauvegarde du nombre de coups joués.\n");
    else
        printf("Erreur lors de la sauvegarde de la grille de jeu.\n");
}


void hint(grid g)
{
    char hint;
    hint = bestStep (g);
    printf("Indication : Essayez %c\n", hint);
}


void finishGame(grid * g, int step)
{
    char next;
    grid coord = init_grid(g->size);

    while (!checkForWin(*g))
    {
        system("clear");
        next = bestStep(*g);
        spreadColor(g, &coord, next);
        step++;
        printf("Couleur saisie : %c\n", next);
        printf("Etape : %i\n", step);
        printGrid(*g);
        printf("\n");
        sleep(1);
    }
}
